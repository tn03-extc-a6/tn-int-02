//Library code
#ifndef _SOLDIER_H_
#define _SOLDIER_H_
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <ctype.h>
# define PORT 4444
char UID[]="0F";
char id[]="10";
char DEST[]="FF";
//packet: id|UID|DEST|time|message id|data len|data

char buff[7][40];
struct soldier_data
{
    char id[2];
    char src[2];
    char dest[2];
    int hr, min, sec;
    int msgid;
    char data[100];
    char name[100];
    
    int temp, datalen, status;
    int pulse_rate;
    float lon,lat;
};



//Random function
void Random(struct soldier_data *D)
{
    int temp,pulse_rate, C;
    float  Cordinate;
    int lower = 30, upper = 40, l=0, h= 9999;
    int low = 60, high = 99;
    float lon = 80.00000, lat = -45.00000;
    int a = 0, b = 1;
    srand(time(NULL));
        temp = (rand() % (upper-lower+1)) + lower;
        pulse_rate = (rand() % (high-low+1)) + low;
        D->temp = temp;
        D->pulse_rate = pulse_rate;
       
        C =(rand() % (h-l+1)) + l;
        switch (a)
        {
            case 0:
            {
                Cordinate = (float)C / 100000;
                lon = lon + Cordinate;
                lat = lat + Cordinate;
                a = 1;
                break;
            }
            case 1:
            {
                Cordinate = (float)C / 100000;
                lon = lon - Cordinate;
                lat = lat - Cordinate;
                a = 0;
                break;
            }
        }
         D->lon = lon;
         D->lat = lat; 
}

//Getting time
char *time_stamp(char *t)
{
	char c=':';
	time_t s, val = 1;
	struct tm* current_time;
	s = time(NULL);
	current_time = localtime(&s);
	sprintf(t,"%02d%02d%02d",current_time->tm_hour,current_time->tm_min,current_time->tm_sec);
	return t;
	printf("%s\n",t);
}

//Encode code(packet creation)
void encode(struct soldier_data *data)
{
    char t[8];
    char a='0';
    for(int i=1;i<=6;i++)
    {
        strcat(buff[i],id);
        strcat(buff[i],UID);
        strcat(buff[i],DEST);
        strcat(buff[i],time_stamp(t));
        switch (i)
        {   
            case 1:
            {
                int len=strlen(data->name);
                char T[20],c='|';
                sprintf(T,"%c%d%d%c",a,i,len,c);
                strcat(T,data->name);
                strcat(buff[i],T);
                break;
            }            
            case 2:
            {
                int len=2;
                char T[20],c='|';
                sprintf(T,"%c%d%d%c%d",a,i,len,c,data->temp);
                strcat(buff[i],T);
                break;
            }
            case 3:
            {
                int len=2;
                char T[20],c='|';
                sprintf(T,"%c%d%d%c%d",a,i,len,c,data->pulse_rate);
                strcat(buff[i],T);
                break;
            }
            case 4:
            {
                int len=18;
                char T[20],c='|';
                sprintf(T,"%c%d%d%c%0.5f%0.5f",a,i,len,c,data->lon,data->lat);
                strcat(buff[i],T);
                break;
            }
            case 5:
            {
                int len=1;
                data->status=0;
                char T[20],c='|';
                sprintf(T,"%c%d%d%c%d",a,i,len,c,data->status);
                strcat(buff[i],T);
                break;
            }
            case 6:
            {
                int len=strlen(data->data);
                char T[200],c='|';
                sprintf(T,"%c%d%d%c",a,i,len,c);
                strcat(T,data->data);
                strcat(buff[i],T);
                break;
            } 
        }
        printf("packet[%d] %s\n",i,buff[i]);
    }
}

void decode(char *str1, struct soldier_data *d)
{
        char res[9][999];
        int j=0;
        for(int i=0; i<7; i++)
        {
            res[i][0] = *((str1)+j);
            res[i][1] = *((str1)+(j+1));
            res[i][2] = '\0';
            j=j+2;
        }
        
        int k=0;
        for(int i=j; *((str1)+i) != '|'; i++)
        {
           res[7][k] = *((str1)+i);
           k++;
        }
        res[7][k] ='\0';
        
        //assigning structure datalen
        d->datalen = atoi(res[7]);
        
        //storing data in res[8]
        for(int i=0; i<d->datalen; i++)
        {
            res[8][i] = *((str1)+k+j+i+1);
            res[8][i+1] = '\0';
        }
        
        //assigning structure id
        for(int m=0; m<2; m++)
            d->id[m] = res[0][m];
        d->id[2] = '\0';
        
        //assigning structure source
        d->src[0] = res[1][0];
        d->src[1] = res[1][1];
        
        //assigning structure destination
        for(int m=0; m<2; m++)
            d->dest[m] = res[2][m];
        d->dest[2] = '\0';
      
        //assigning structure time in hr,min,sec 
        d->hr = atoi(res[3]);
        d->min = atoi(res[4]);
        d->sec = atoi(res[5]);
        
        //assigning structure msgid
        d->msgid = atoi(res[6]);
        switch(d->msgid)
        {
            case 1 : //For Name packet
            {
                for(int i=0; i<d->datalen; i++)
                {
                    d->name[i] = res[8][i];
                }
                break;
            }
            case 2 : //For Temperature packet 
            {
                d->temp = atoi(res[8]);
                break;
            }
            case 3 : //For Pulse rate packet
            {
                d->pulse_rate = atoi(res[8]);
                break;
            }
            case 4 : //For GPS packet
            {
                int i=0;
                char Long[10], Lat[10];
                while(res[8][i]!='.')
                {
                    Long[i]=res[8][i];
                    i++;
                }
                
                Long[i] = res[8][i];
                for(int j=i; j<8; j++)
                {
                    Long[j] = res[8][i];
                    i++;
                    Long[i]='\0';
                }
                
                d->lon = atof(Long);
                int count=0;
                while(res[8][i] != '.')
                {
                    strncat(Lat, &res[8][i], 1);
                    i++;
                    count++;
                }
                Lat[count] = res[8][i];
                for(int j=count; j<(count+6); j++)
                {
                    Lat[j] = res[8][i];
                    i++;
                }
                
                d->lat = atof(Lat);
                break;
            }
            case 5 : //For SOS packet
            {
                d->status = atoi(res[8]);
                break;
            }
            case 6 : //For Text packet
            {
                for(int i=0; i<d->datalen; i++)
                {
                    d->data[i] = res[8][i];
                    d->data[i+1]='\0';
                }
                break;
            }
            
        }
}


#endif